package net.pl3x.bukkit.fangs;

import net.pl3x.bukkit.fangs.command.CmdFangs;
import net.pl3x.bukkit.fangs.configure.Config;
import net.pl3x.bukkit.fangs.configure.Lang;
import net.pl3x.bukkit.fangs.listener.PlantListener;
import net.pl3x.bukkit.fangs.listener.ShootListener;
import net.pl3x.bukkit.fangs.listener.SummonListener;
import org.bukkit.plugin.java.JavaPlugin;

public class Fangs extends JavaPlugin {
    private final FangManager fangManager;

    public Fangs() {
        fangManager = new FangManager(this);
    }

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        getServer().getPluginManager().registerEvents(new PlantListener(this), this);
        getServer().getPluginManager().registerEvents(new ShootListener(this), this);
        getServer().getPluginManager().registerEvents(new SummonListener(this), this);

        getCommand("fangs").setExecutor(new CmdFangs(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getFangManager().cancelAllCooldowns();

        Logger.info(getName() + " disabled.");
    }

    public static Fangs getPlugin() {
        return Fangs.getPlugin(Fangs.class);
    }

    public FangManager getFangManager() {
        return fangManager;
    }
}
