package net.pl3x.bukkit.fangs;

import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.EntityEvokerFangs;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.MathHelper;
import net.pl3x.bukkit.fangs.configure.Config;
import net.pl3x.bukkit.fangs.task.SummonCooldown;
import net.pl3x.bukkit.fangs.util.AABB;
import net.pl3x.bukkit.fangs.util.Ray;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class FangManager {
    private final Fangs plugin;
    private final Map<UUID, SummonCooldown> summonCooldowns = new HashMap<>();

    public FangManager(Fangs plugin) {
        this.plugin = plugin;
    }

    public boolean isOnCooldown(UUID uuid) {
        return summonCooldowns.containsKey(uuid);
    }

    public void startCooldown(UUID uuid) {
        SummonCooldown summonCooldownTask = new SummonCooldown(this, uuid);
        summonCooldowns.put(uuid, summonCooldownTask);
        summonCooldownTask.runTaskLater(plugin, Config.SUMMON_COOLDOWN * 20);
    }

    public void stopCooldown(UUID uuid) {
        SummonCooldown summonCooldownTask = summonCooldowns.remove(uuid);
        if (summonCooldownTask != null) {
            summonCooldownTask.cancel();
        }
    }

    public void cancelAllCooldowns() {
        summonCooldowns.values().forEach(BukkitRunnable::cancel);
        summonCooldowns.clear();
    }

    public void attackTarget(Player player, LivingEntity target, String reason) {
        Location playerLoc = player.getLocation();
        double originX = playerLoc.getX();
        double originY = playerLoc.getY();
        double originZ = playerLoc.getZ();

        EntityLiving entityliving = ((CraftLivingEntity) target).getHandle();

        double minY = Math.min(entityliving.locY, originY);
        double maxY = Math.max(entityliving.locY, originY) + 1.0D;
        float yaw = (float) MathHelper.c(entityliving.locZ - originZ, entityliving.locX - originX);
        int warmUp;
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        if (entityPlayer.h(entityliving) < 9.0D) {
            float delta;
            for (warmUp = 0; warmUp < 5; ++warmUp) {
                delta = yaw + (float) warmUp * (float) Math.PI * 0.4F;
                spawnFangs(entityPlayer, originX + (double) MathHelper.cos(delta) * 1.5D, originZ + (double) MathHelper.sin(delta) * 1.5D, minY, maxY, delta, 0, reason);
            }
            for (warmUp = 0; warmUp < 8; ++warmUp) {
                delta = yaw + (float) warmUp * (float) Math.PI * 2.0F / 8.0F + 1.2566371F;
                spawnFangs(entityPlayer, originX + (double) MathHelper.cos(delta) * 2.5D, originZ + (double) MathHelper.sin(delta) * 2.5D, minY, maxY, delta, 3, reason);
            }
        } else {
            for (warmUp = 0; warmUp < 16; ++warmUp) {
                double offset = 1.25D * (double) (warmUp + 1);
                spawnFangs(entityPlayer, originX + (double) MathHelper.cos(yaw) * offset, originZ + (double) MathHelper.sin(yaw) * offset, minY, maxY, yaw, warmUp, reason);
            }
        }

    }

    public void attackLocation(Player player, Location location, String reason) {
        double originX = location.getX();
        double originY = location.getY();
        double originZ = location.getZ();

        double minY = originY - 8;
        double maxY = originY + 8;
        float yaw = location.getYaw();
        int warmUp;
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        float delta;
        spawnFangs(entityPlayer, originX, originZ, minY, maxY, location.getYaw(), 0, reason);
        for (warmUp = 0; warmUp < 5; ++warmUp) {
            delta = yaw + (float) warmUp * (float) Math.PI * 0.4F;
            spawnFangs(entityPlayer, originX + (double) MathHelper.cos(delta) * 1.5D, originZ + (double) MathHelper.sin(delta) * 1.5D, minY, maxY, delta, 0, reason);
        }
        for (warmUp = 0; warmUp < 8; ++warmUp) {
            delta = yaw + (float) warmUp * (float) Math.PI * 2.0F / 8.0F + 1.2566371F;
            spawnFangs(entityPlayer, originX + (double) MathHelper.cos(delta) * 2.5D, originZ + (double) MathHelper.sin(delta) * 2.5D, minY, maxY, delta, 3, reason);
        }
    }

    public void spawnFangs(Player player, double x, double z, double minY, double maxY, float yaw, int warmUp, String reason) {
        spawnFangs(((CraftPlayer) player).getHandle(), z, x, minY, maxY, yaw, warmUp, reason);
    }

    public void spawnFangs(EntityLiving owner, double x, double z, double minY, double maxY, float yaw, int warmUp, String reason) {
        BlockPosition blockposition = new BlockPosition(x, maxY, z);
        boolean flag = false;
        double d4 = 0.0D;

        do {
            if (!owner.world.d(blockposition, true) && owner.world.d(blockposition.down(), true)) {
                if (!owner.world.isEmpty(blockposition)) {
                    AxisAlignedBB axisalignedbb = owner.world.getType(blockposition).d(owner.world, blockposition);
                    if (axisalignedbb != null) {
                        d4 = axisalignedbb.e;
                    }
                }

                flag = true;
                break;
            }

            blockposition = blockposition.down();
        } while (blockposition.getY() >= MathHelper.floor(minY) - 1);

        if (flag) {
            spawnFangs(owner, x, (double) blockposition.getY() + d4, z, yaw, warmUp, reason);
        }

    }

    public void spawnFangs(LivingEntity owner, double x, double y, double z, float yaw, int warmUp, String reason) {
        spawnFangs(((CraftLivingEntity) owner).getHandle(), x, y, z, yaw, warmUp, reason);
    }

    public void spawnFangs(EntityLiving owner, double x, double y, double z, float yaw, int warmUp, String reason) {
        EntityEvokerFangs fangs = new EntityEvokerFangs(owner.world, x, y, z, yaw, warmUp, owner);
        owner.world.addEntity(fangs);

        fangs.getBukkitEntity().setMetadata(reason, new FixedMetadataValue(plugin, true));
    }

    public LivingEntity getTarget(Player player, int max) {
        List<LivingEntity> possible = player.getNearbyEntities(max, max, max).stream()
                .filter(entity -> entity instanceof LivingEntity)
                .filter(player::hasLineOfSight)
                .map(entity -> (LivingEntity) entity)
                .collect(Collectors.toList());
        if (possible == null || possible.isEmpty()) {
            return null;
        }
        Ray ray = Ray.from(player);
        double d = -1;
        LivingEntity closest = null;
        for (LivingEntity target : possible) {
            double dis = AABB.from(target).collidesD(ray, 0, max);
            if (dis != -1) {
                if (dis < d || d == -1) {
                    d = dis;
                    closest = target;
                }
            }
        }
        return closest;
    }
}
