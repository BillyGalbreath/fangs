package net.pl3x.bukkit.fangs.command;

import net.pl3x.bukkit.fangs.FangManager;
import net.pl3x.bukkit.fangs.Fangs;
import net.pl3x.bukkit.fangs.configure.Config;
import net.pl3x.bukkit.fangs.configure.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdFangs implements TabExecutor {
    private final Fangs plugin;

    public CmdFangs(Fangs plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.fangs.reload")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Config.reload();
            Lang.reload();

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.fangs")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (!Config.SUMMON_ENABLED) {
            Lang.send(sender, Lang.SUMMON_DISABLED);
            return true;
        }

        Player player = (Player) sender;
        FangManager fangManager = plugin.getFangManager();
        if (fangManager.isOnCooldown(player.getUniqueId())) {
            Lang.send(sender, Lang.SUMMON_ON_COOLDOWN);
            return true;
        }

        // get target livingEntity
        LivingEntity target = fangManager.getTarget(player, 16);
        if (target == null) {
            Lang.send(sender, Lang.SUMMON_NO_TARGET);
            return true;
        }

        fangManager.startCooldown(player.getUniqueId());
        fangManager.attackTarget(player, target, "FangsSummon");
        return true;
    }
}
