package net.pl3x.bukkit.fangs.task;

import net.pl3x.bukkit.fangs.FangManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class SummonCooldown extends BukkitRunnable {
    private final FangManager fangManager;
    private final UUID uuid;

    public SummonCooldown(FangManager fangManager, UUID uuid) {
        this.fangManager = fangManager;
        this.uuid = uuid;
    }

    @Override
    public void run() {
        fangManager.stopCooldown(uuid);
    }
}
