package net.pl3x.bukkit.fangs.configure;

import net.pl3x.bukkit.fangs.Fangs;
import net.pl3x.bukkit.fangs.Logger;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;

    public static boolean SUMMON_ENABLED;
    public static boolean SUMMON_REQUIRE_SNEAK_SHIFT;
    public static int SUMMON_COOLDOWN;
    public static Material SUMMON_TOOL;

    public static boolean PLANT_ENABLED;
    public static Material PLANT_BASE_TYPE;
    public static int PLANT_BASE_DATA;

    public static void reload() {
        Fangs plugin = Fangs.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");

        SUMMON_ENABLED = config.getBoolean("summon.enabled", true);
        SUMMON_REQUIRE_SNEAK_SHIFT = config.getBoolean("summon.require-sneak-shift", true);
        SUMMON_COOLDOWN = config.getInt("summon.cooldown", 2);
        try {
            SUMMON_TOOL = Material.matchMaterial(config.getString("summon.tool", "IRON_NUGGET"));
        } catch (IllegalArgumentException e) {
            Logger.warn("Invalid summon.tool specified. Defaulting to IRON_NUGGET.");
            SUMMON_TOOL = Material.IRON_NUGGET;
        }

        PLANT_ENABLED = config.getBoolean("plant.enabled", true);
        try {
            PLANT_BASE_TYPE = Material.getMaterial(config.getString("plant.base.type", "STONE").toUpperCase());
        } catch (IllegalArgumentException e) {
            Logger.warn("Invalid plant.base.type specified. Defaulting to STONE.");
            PLANT_BASE_TYPE = Material.STONE;
        }
        PLANT_BASE_DATA = config.getInt("plant.base.data", 2);
    }
}
